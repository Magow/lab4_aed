#include "grafo.h"
// pasar todo a español 
// ctrl F
// 1 debe ser recursivo 
// 2 hacer versión con stacks 

GrafoTipo::GrafoTipo(int maxV){
   numVertices = 0;
   maxVertices = maxV;  
   vertices = new int[maxV];
   edges = new int*[maxV];
 
   for(int i = 0; i < maxV; i++)
     edges[i] = new int[maxV];
 
   marks = new bool[maxV];
}

GrafoTipo::~GrafoTipo(){
   delete [] vertices;// liberar espacio 
 
   for(int i = 0; i < maxVertices; i++)
      delete [] edges[i];
 
   delete [] edges;
   delete [] marks;
} 

void GrafoTipo::AddVertex(int vertex){
   vertices[numVertices] = vertex;

   for(int index = 0; index < numVertices; index++) {
     edges[numVertices][index] = NULL_EDGE;
     edges[index][numVertices] = NULL_EDGE;
   }
   
   numVertices++;
}

void GrafoTipo::AddEdge(int fromVertex, int toVertex, int weight){
   int row, col;

   row = IndexIs(vertices, fromVertex);
   col = IndexIs(vertices, toVertex);
   edges[row][col] = weight;
} 

int GrafoTipo::WeightIs(int fromVertex, int toVertex){
   int row, col;

   row = IndexIs(vertices, fromVertex);
   col = IndexIs(vertices, toVertex);
   
   return edges[row][col];
} 

int GrafoTipo::IndexIs(int *vertices, int vertex){
  for(int i = 0; i < numVertices; i++){
      if (vertices[i] == vertex)
          return i;
  }

  return -1;
}

bool GrafoTipo::IsFull(){
  if (numVertices == maxVertices)
      return true;
  
  return false;
}

bool GrafoTipo::IsEmpty(){
  if (numVertices == 0)
      return true;
  
  return false;
}

int GrafoTipo::GraphSize(){
  return numVertices;
}
