#include <iostream> 
#include "grafo.h" 

using namespace std; 

int main() { 
    GrafoTipo x(10); 

    cout << "Vació? " << x.IsEmpty() << " - " << x.GraphSize() << endl;

    x.AddVertex(10);
    x.AddVertex(20);

    cout << "\nVació? " << x.IsEmpty() << " - " << x.GraphSize() << endl;
    x.AddEdge(10, 20, 100);

    cout << "Peso: " << x.WeightIs(10, 20) << endl;
    x.AddVertex(30);

    cout << "\nVació? " << x.IsEmpty() << " - " << x.GraphSize() << endl;
    x.AddEdge(10, 30, 200);
    cout << "Peso: " << x.WeightIs(10, 30) << endl;
    x.AddVertex(40);
    cout << "\nVació? " << x.IsEmpty() << " - " << x.GraphSize() << endl;
    x.AddVertex(44);
    x.AddEdge(10, 40, 10000);

    cout << "Peso: " << x.WeightIs(10, 40) << endl;
    cout << "\nVació? " << x.IsEmpty() << " - " << x.GraphSize() << endl;
}